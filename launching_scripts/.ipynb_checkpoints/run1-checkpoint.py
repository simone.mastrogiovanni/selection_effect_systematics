import icarogw
import pickle
import bilby
from icarogw.analyses.cosmo_pop_rate_marginalized import hierarchical_analysis

########### INPUT BOX ############
mass_model ='BBH-powerlaw-gaussian' # the mass model
rate_model = 'madau' # the rate evol model

# SNR and IFAR cut
snr_cut = 0.
ifar_cut = 4.0
#################################

outdir='../run1'
print('Writing in ', outdir)
injections= pickle.load(open('../O3_injections.p', 'rb' ))
injections.update_cut(snr_cut=snr_cut,ifar_cut=ifar_cut)
pos_dict=pickle.load(open('../run1_posteriors.p', 'rb' ))

prior_dict=icarogw.utils.quick_init.initialize_prior_dict(mass_model,'flatLCDM',rate_model,False)
prior_dict['H0']=67.9
prior_dict['Om0']=0.3065
prior_dict['mmax'].maximum=200
prior_dict['mmax'].minimum=50
prior_dict['zp'].maximum=4.
prior_dict['zp'].minimum=0.
prior_dict['gamma'].maximum=12.
prior_dict['gamma'].minimum=0.
prior_dict['kappa'].maximum=6.
prior_dict['kappa'].minimum=0.
prior_dict['R0'] = bilby.core.prior.Uniform(0,100,name='R0')
analysis = hierarchical_analysis(pos_dict,injections,scale_free=False)
result = analysis.run_bilby(mass_model,'flatLCDM',rate_model,prior_dict,parallel=2048,nlive=1000,npool=16,outdir=outdir,label='dynesty')
result.plot_corner()
